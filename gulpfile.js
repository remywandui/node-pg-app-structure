'use strict';

var gulp       = require('gulp'),
    mocha      = require('gulp-mocha'),
    concat     = require('gulp-concat'),
    jshint     = require('gulp-jshint'),
    uglify     = require('gulp-uglify'),
    rimraf     = require('gulp-rimraf'),
    changed    = require('gulp-changed'),
    imagemin   = require('gulp-imagemin'),
    minifyCSS  = require('gulp-minify-css'),
    browserify = require('gulp-browserify'),
    minifyHTML = require('gulp-minify-html'),
    stripDebug = require('gulp-strip-debug'),
    autoPrefix = require('gulp-autoprefixer'),
    
    /////////////////////////////////////////////
    // Constants 
    /////////////////////////////////////////////
    TEST_SRC   = './test/string.js',
    SRC        = './app/*.js',
    DEST       = './dist';
    
// check for changes in JS files and run tests
gulp.task('watch', ['mocha'], function () {
    gulp.watch(['./src/scripts/*.js'], ['mocha']);
});

// remove all unnecessary files
gulp.task('clean', function () {
    return gulp.src(DEST)
        .pipe(rimraf());
});

// bundle code for production
gulp.task('build', ['test', 'clean'], function () {
    gulp.src(['./src/scripts/*.js'])
        .pipe(uglify())
        .pipe(gulp.dest(DEST));
});

// check for semantic errors
gulp.task('jshint', function () {
    gulp.src('./src/scripts/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// minify images
gulp.task('imagemin', function () {
    var imgesrc = './src/images/**/*',
        imgedst = './build/images';
    
    gulp.src(imgesrc)
        .pipe(changed(imgedst))
        .pipe(imagemin())
        .pipe(gulp.dest(imgedst));
});

// minify html pages
gulp.task('minify-html', function () {
    var htmlsrc  = './src/html/*.html',
        htmldest = './build';
    
    gulp.src(htmlsrc)
        .pipe(changed(htmldest))
        .pipe(minifyHTML())
        .pipe(gulp.dest(htmldest));
});

// strip debugging an minify. index.js first, then 
// all other js files in any order
gulp.task('strip', function () {
    gulp.src(['./src/scripts/index.js', './src/scripts/*.js'])
        .pipe(concat('index.js'))
        .pipe(stripDebug())
        .pipe(uglify())
        .pipe(gulp.dest('./build/scripts'));
});

// css concat, auto-prefix and minify
gulp.task('minify-css', function () {
    gulp.src(['./src/styles/*.css'])
        .pipe(concat('styles.css'))
        .pipe(autoPrefix('last 2 versions'))
        .pipe(minifyCSS())
        .pipe(gulp.dest('./build/styles'));
});

// code for testing the JS files
gulp.task('mocha', function () {
    var error = false;
    return gulp.src('./test/*')
        .pipe(mocha({reporter: 'dot'}))
        .on('error', function () {
            console.log('Tests failed!');
            error = true;
        })
        .on('end', function () {
            if (!error) {
                console.log('Tests succeeded!');
                process.exit(0);
            }
        });
});

// default gulp task for running any number of dependent subtasks
gulp.task('default', ['jshint', 'imagemin', 'minify-html', 'minify-css'], function () {
    // watch for html changes
    gulp.watch('./src/html/*.html', function () {
        gulp.run('minify-html');
    });
    
    // watch for JS changes
    gulp.watch('./src/scripts/*.js', function () {
        gulp.run('jshint', 'strip');
    });
    
    // watch for CSS changes
    gulp.watch('./src/styles/*.css', function () {
        gulp.run('minify-css');
    });
});
