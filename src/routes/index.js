var express = require('express');
var app     = express.Router();
var birds   = require('./birds');
var users   = require('./users');

// Test that the routes pack is fine
app.get('/', function(req, res) {
    res.send("Got a GET request to the home URL");
});

app.use('/birds', birds);
app.use('/users', users);

module.exports = app;