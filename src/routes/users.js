'use strict';

var db = require('../db').db;

var express = require('express');
var router  = express.Router();

// middleware that is specific to this api
router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now());
    next();
});
// define the home page route
router.get('/', (req, res) => {
    res.send('Users home page');
});

// define the about route
router.get('/about', (req, res) => {
    res.send('About users');
});

router.get('/all', (req, res) => {
    var handler = db.users.all;
    handler(req)
        .then(data => {
            console.log("Success! ", data);
            res.json({success: true, data});
        })
        .catch(error => {
            res.json({success: true, error: error.message || error});
        });
});

module.exports = router;